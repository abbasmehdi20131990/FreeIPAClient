#!bin/bash/
#§Step 1: Set Proper hostname and FQDN
sudo hostnamectl set-hostname ipa.example.com
 hostname -f
#§Step 2:Update your Ubuntu server
sudo apt -y update
sudo apt -y -n upgrade
#§Step 3: Install rng-tools
sudo apt -y install rng-tools
sudo tee /etc/default/rng-tools<<EOF
  GNU nano 4.8                 /etc/default/rng-tools                 Modified  
# Configuration for the rng-tools initscript
# $Id: rng-tools.default,v 1.1.2.5 2008-06-10 19:51:37 hmh Exp $

# This is a POSIX shell fragment

# Set to the input source for random data, leave undefined
# for the initscript to attempt auto-detection.  Set to /dev/null
# for the viapadlock and tpm drivers.
#HRNGDEVICE=/dev/hwrng
#HRNGDEVICE=/dev/null
HRNGDEVICE=/dev/urandom
# Additional options to send to rngd. See the rngd(8) manpage for
# more information.  Do not specify -r/--rng-device here, use
# HRNGDEVICE for that instead.
#RNGDOPTIONS="--hrng=intelfwh --fill-watermark=90% --feed-interval=1"
#RNGDOPTIONS="--hrng=viakernel --fill-watermark=90% --feed-interval=1"
#RNGDOPTIONS="--hrng=viapadlock --fill-watermark=90% --feed-interval=1"
#RNGDOPTIONS="--hrng=tpm --fill-watermark=90% --feed-interval=1"
EOF
sudo systemctl enable rng-tools
sudo systemctl start rng-tools
#§Step 4: Install FreeIPA Server on Ubuntu 18.04/16.04
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
sudo apt-get install freeipa-client oddjob-mkhomedir -y 
sudo tee /usr/share/pam-configs/mkhomedir <<EOF
 Name: create home directory FreeIPA
 Default: yes
 Priority: 900
 Session-Type: Additional
 Session:
 required pam_mkhomedir.so umask=0022 skel=/etc/skel
EOF
sudo pam-auth-update
#§Step 5: Configure Firewall for FreeIPA
sudo ufw enable
for i in 80 443 389 636 88 464; do sudo ufw allow proto tcp from any to any port $i; done
for i in 88 464 123; do sudo ufw allow proto udp from any to any port $i; done
sudo ufw reload
